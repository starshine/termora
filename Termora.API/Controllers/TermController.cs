using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Termora.Core;

using ILogger = Serilog.ILogger;

namespace Termora.API;

[ApiController]
[Route("/v2/terms/[controller]")]
public class TermController : ControllerBase
{
    private readonly ILogger _logger;
    private readonly TermoraContext _context;
    
    public TermController(ILogger logger, TermoraContext context)
    {
        _logger = logger;
        _context = context;
    }
    
    [HttpGet(Name = "GetTerms")]
    public async Task<IActionResult> GetTerms()
    {
        var terms = await _context.Terms.Where(t => !t.Flags.HasFlag(TermFlags.ListHidden)).ToListAsync();

        return Ok(terms);
    }
}