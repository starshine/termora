using Discord.Interactions;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Termora.Core;

namespace Termora.Bot;

[Group("termora", "Meta commands")]
public class MiscCommands : InteractionModuleBase<ShardedInteractionContext>
{
    private readonly ILogger _logger;
    private readonly TermoraContext _database;

    public MiscCommands(ILogger logger, TermoraContext database)
    {
        _logger = logger;
        _database = database;
    }

    [SlashCommand("stats", "Ping pong!")]
    public async Task Stats()
    {
        var termCount = await _database.Terms.CountAsync();

        await RespondAsync($"Pong! {Context.Client.Latency:N0}ms\n\n{termCount:N0} terms");
    }
}