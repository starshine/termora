using Discord;
using Serilog;

using Termora.Core;

namespace Termora.Bot;

public class EmbedService
{
    private readonly BotConfig _config;

    public EmbedService(BotConfig config)
    {
        _config = config;
    }

    public Embed CreateTermEmbed(Term term)
    {
        var builder = CreateBuilder()
            .WithTitle(term.Names[0])
            .WithDescription(term.Description)
            .WithFooter($"ID: {term.Id}")
            .WithTimestamp(term.CreatedAt.ToDateTimeOffset());

        if (_config.WebsiteBase != null)
            builder.WithUrl($"{_config.WebsiteBase}/term/{term.Id}");

        if (term.Names.Length > 0)
            builder.AddField(
                term.Names.Length > 2 ? "Synonyms" : "Synonym",
                string.Join(",", term.Names.Skip(1))
            );

        builder.AddField("Source", term.Source);

        if (term.Note != null)
            builder.AddField("Note", term.Note);

        return builder.Build();
    }

    public Embed CreateGenericEmbed(string? title = null, string? description = null, string? footer = null)
    {
        if (title == null && description == null)
            throw new ArgumentNullException("Either title or description must be set");

        var embed = CreateBuilder();
        if (title != null)
            embed.WithTitle(title);
        if (description != null)
            embed.WithDescription(description);
        if (footer != null)
            embed.WithFooter(footer);

        return embed.Build();
    }

    private EmbedBuilder CreateBuilder() => new EmbedBuilder().WithColor(_config.EmbedColour);
}