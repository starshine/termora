using System.Reflection;
using Discord.Addons.Hosting;
using Discord.Addons.Hosting.Util;
using Discord.Interactions;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using ILogger = Serilog.ILogger;

namespace Termora.Bot;

public class InteractionHandler : DiscordShardedClientService
{
    private IServiceProvider _provider;
    private InteractionService _interactionService;
    private ILogger _logger;
    private ulong? _commandsGuildId;

    public InteractionHandler(
        DiscordShardedClient client,
        Microsoft.Extensions.Logging.ILogger<InteractionHandler> msLogger,
        ILogger logger,
        IServiceProvider provider,
        InteractionService interactionService,
        BotConfig config) : base(client, msLogger)
    {
        _provider = provider;
        _interactionService = interactionService;
        _logger = logger;
        _commandsGuildId = config.CommandsGuildId;

        client.InteractionCreated += InteractionCreate;
    }

    private async Task InteractionCreate(SocketInteraction interaction)
    {
        var ctx = new ShardedInteractionContext(Client, interaction);
        await _interactionService.ExecuteCommandAsync(ctx, _provider);
    }

    protected override async Task ExecuteAsync(CancellationToken ct)
    {
        await this.Client.WaitForReadyAsync(ct);

        _logger.Information("Registering command modules");
        var modules = await _interactionService.AddModulesAsync(Assembly.GetEntryAssembly(), _provider);
        _logger.Information("Registered {Count} command modules", modules.Count());

        if (_commandsGuildId != null)
        {
            _logger.Information("Registering commands in guild {Id}", _commandsGuildId);
            await _interactionService.RegisterCommandsToGuildAsync(_commandsGuildId.Value, deleteMissing: true);
            _logger.Information("Registered commands in guild {Id}", _commandsGuildId);
        }
        else
        {
            _logger.Information("Registering commands globally");
            await _interactionService.RegisterCommandsGloballyAsync(deleteMissing: true);
            _logger.Information("Registered commands globally");
        }
    }
}