namespace Termora.Bot;

public static class MiscExtensions
{
    public static string AsCode(this string src) => $"``{src.EscapeBacktickPair()}``";

    // String escape method taken from PluralKit:
    // https://github.com/PluralKit/PluralKit/blob/8717f964d3d639c181aef21f25318b4d42331da5/PluralKit.Bot/Utils/DiscordUtils.cs#L159-L175
    public static string EscapeBacktickPair(this string input)
    {
        // Break all pairs of backticks by placing a ZWNBSP (U+FEFF) between them.
        // Run twice to catch any pairs that are created from the first pass
        var escaped = input
            .Replace("``", "`\ufeff`")
            .Replace("``", "`\ufeff`");

        // Escape the start/end of the string if necessary to better "connect" with other things
        if (escaped.StartsWith("`")) escaped = "\ufeff" + escaped;
        if (escaped.EndsWith("`")) escaped = escaped + "\ufeff";

        return escaped;
    }
}