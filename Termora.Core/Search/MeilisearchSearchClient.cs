using Meilisearch;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Serilog.Core;

namespace Termora.Core;

public class MeilisearchSearchClient : ISearchClient
{
    private readonly ILogger _logger;
    private readonly MeilisearchClient _client;
    private readonly TermoraContext _context;

    public MeilisearchSearchClient(ILogger logger, CoreConfig config, TermoraContext context)
    {
        _logger = logger;
        _context = context;

        _client = new MeilisearchClient(config.MeilisearchUrl, config.MeilisearchKey);
    }

    public async ValueTask InitTermsAsync()
    {
        _logger.Information("Recreating terms index");
        
        // delete existing index to ensure we don't have any outdated terms
        await _client.Index("terms").DeleteAsync();

        var terms = await _context.Terms.ToListAsync();

        await _client.Index("terms").AddDocumentsAsync(terms);
        
        _logger.Information("Recreated terms index!");
    }

    public async Task<IEnumerable<Term>> SearchAsync(string query)
    {
        _logger.Debug("Searching for {Query}", query);
    
        var index = _client.Index("terms");

        var searchQuery = new SearchQuery()
        {
            HighlightPreTag = "*",
            HighlightPostTag = "*",
        };

        var results = await index.SearchAsync<Term>(query, searchQuery);
        return results.Hits;
    }
}