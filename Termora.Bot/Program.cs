﻿using Discord;
using Discord.Addons.Hosting;
using Discord.Interactions;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using Microsoft.EntityFrameworkCore;
using Termora.Core;
using Fergun.Interactive;

namespace Termora.Bot;

public static class Program
{
    public static async Task Main(string[] args)
    {
        using var logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .WriteTo.Console(theme: AnsiConsoleTheme.Code)
            .CreateLogger();

        logger.Information("Hello world!");

        var host = Host.CreateDefaultBuilder(args)
            .UseSerilog(logger)
            .ConfigureDiscordShardedHost((context, config) =>
            {
                config.SocketConfig = new DiscordSocketConfig
                {
                    LogLevel = LogSeverity.Verbose,
                    GatewayIntents = GatewayIntents.Guilds,
                    LogGatewayIntentWarnings = false,
                };

                // BotConfig is not available yet, so we have to parse the section manually :(
                config.Token = context.Configuration.GetRequiredSection("Bot").GetValue<string>("Token")!;
            })
            .UseInteractionService((context, config) =>
            {
                config.DefaultRunMode = RunMode.Async;
                config.LogLevel = LogSeverity.Info;
                config.UseCompiledLambda = true;
            })
            .ConfigureServices((context, services) =>
            {
                // parse configuration into classes and add them to the host
                services.AddSingleton<CoreConfig>(
                    context.Configuration.GetRequiredSection("Core").Get<CoreConfig>() ?? new()
                );
                services.AddSingleton<BotConfig>(
                    context.Configuration.GetRequiredSection("Bot").Get<BotConfig>() ?? new()
                );

                // add database context
                services.AddDbContext<TermoraContext>();

                // add search
                services.AddTransient<ISearchClient, MeilisearchSearchClient>();

                // command handler
                services.AddHostedService<InteractionHandler>();

                // add interactive service for pagination
                services.AddSingleton(new InteractiveConfig { DefaultTimeout = TimeSpan.FromMinutes(15) });
                services.AddSingleton<InteractiveService>();

                // misc services
                services.AddTransient<EmbedService>();
            })
            .Build();

        // run migrations + set up search
        // create a scope
        using (var scope = host.Services.CreateScope())
        {
            var log = scope.ServiceProvider.GetRequiredService<ILogger>();

            try
            {
                using var context = scope.ServiceProvider.GetRequiredService<TermoraContext>();

                // check if we have any unapplied migrations (for logging purposes)
                var migrations = (await context.Database.GetPendingMigrationsAsync()).ToList();
                if (migrations.Any())
                {
                    await context.Database.MigrateAsync();

                    log.Information("Applied {Count} migrations!", migrations.Count);
                }
                else
                {
                    log.Information("There are no pending migrations");
                }
            }
            catch (Exception e)
            {
                log.Fatal(e, "Could not migrate database");
                return;
            }

            try
            {
                var search = scope.ServiceProvider.GetRequiredService<ISearchClient>();
                await search.InitTermsAsync();
            }
            catch (Exception e)
            {
                log.Fatal(e, "Could not init search index");
                return;
            }
        }

        await host.RunAsync();
    }
}