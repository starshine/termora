using NodaTime;

namespace Termora.Core;

public class Topic
{
    public int Id { get; init; }
    public string[] Names { get; set; } = null!;
    public string Description { get; set; } = null!;

    public Instant CreatedAt { get; set; }
}