namespace Termora.Core;

public interface ISearchClient
{
    public ValueTask InitTermsAsync();
    public Task<IEnumerable<Term>> SearchAsync(string query);
}