using Microsoft.EntityFrameworkCore;

using Discord.Interactions;
using Serilog;

using Termora.Core;

namespace Termora.Bot;

public class TopicCommands : InteractionModuleBase<ShardedInteractionContext>
{
    private readonly ILogger _logger;
    private readonly TermoraContext _database;
    private readonly EmbedService _embeds;

    public TopicCommands(ILogger logger, TermoraContext database, EmbedService embeds)
    {
        _logger = logger;
        _database = database;
        _embeds = embeds;
    }

    [SlashCommand("explain", "Explain a topic!")]
    public async Task Explain([Summary(name: "topic", description: "The topic to explain")] string input)
    {
        input = input.ToLower();

        _logger.Debug("Looking up explanation topic {Input}", input);

        var topic = await _database.Topics.Where(t => t.Names.Contains(input)).FirstOrDefaultAsync();
        if (topic == null)
        {
            await RespondAsync($"No explanation topic called {input.AsCode()} found, sorry.", ephemeral: true);
            return;
        }

        await RespondAsync(topic.Description);
    }

    [SlashCommand("topics", "Show a list of all explanation topics!")]
    public async Task Topics()
    {
        var topics = await _database.Topics.ToListAsync();
        if (topics.Count == 0)
        {
            await RespondAsync("There are no explanation topics available, sorry.", ephemeral: true);
            return;
        }

        await RespondAsync(embed: _embeds.CreateGenericEmbed(
            title: "Explanation topics",
            description: string.Join("\n", topics.Select(topic => topic.Names[0]))
        ));
    }
}