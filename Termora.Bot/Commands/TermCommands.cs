using Discord.Interactions;
using Serilog;
using Termora.Core;
using Fergun.Interactive;
using Fergun.Interactive.Extensions;
using Fergun.Interactive.Pagination;

namespace Termora.Bot;

public class TermCommands : InteractionModuleBase<ShardedInteractionContext>
{
    private readonly ILogger _logger;
    private readonly TermoraContext _context;
    private readonly EmbedService _embeds;
    private readonly ISearchClient _search;
    private readonly InteractiveService _interactive;

    public TermCommands(ILogger logger, TermoraContext context, EmbedService embeds, ISearchClient search,
        InteractiveService interactive)
    {
        _logger = logger;
        _context = context;
        _embeds = embeds;
        _search = search;
        _interactive = interactive;
    }

    [SlashCommand("search", "Search for a term")]
    public async Task Search(
        [Summary("Query", "The query to search for")]
        string query,
        [Summary("tag", "The tag to limit searches to")]
        string? tag,
        [Summary("no-cw", "Whether to hide terms with content warnings")]
        bool noCw)
    {
        _logger.Debug("Searching for term {Query}", query);

        var results = (await _search.SearchAsync(query)).ToList();

        switch (results.Count)
        {
            case 0:
                await RespondAsync($"No results found for {query.AsCode()}, sorry.", ephemeral: true);
                return;
            case 1:
                await RespondAsync(embed: _embeds.CreateTermEmbed(results[0]));
                break;
        }

        await RespondAsync("testing beep boop");
    }
}