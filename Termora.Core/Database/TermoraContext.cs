using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

using Npgsql;

namespace Termora.Core;

public class TermoraContext : DbContext
{
    public DbSet<Term> Terms { get; set; } = null!;
    public DbSet<Tag> Tags { get; set; } = null!;
    public DbSet<Topic> Topics { get; set; } = null!;

    private string _connString;

    public TermoraContext(CoreConfig config)
    {
        _connString = new NpgsqlConnectionStringBuilder(config.Database)
        {
            Timeout = 5,
            MaxPoolSize = 500
        }.ConnectionString;
    }

    public TermoraContext(string connString)
    {
        _connString = connString;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder
            .UseNpgsql(_connString, o => o.UseNodaTime())
            .UseSnakeCaseNamingConvention();
}

public class TermoraContextFactory : IDesignTimeDbContextFactory<TermoraContext>
{
    public TermoraContext CreateDbContext(string[] args)
    {
        return new TermoraContext(args[0]);
    }
}