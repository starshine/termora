using NodaTime;

namespace Termora.Core;

public class Term
{
    public int Id { get; init; }
    public string[] Names { get; set; } = null!;
    public string Description { get; set; } = null!;
    public string Source { get; set; } = null!;
    public string? Note { get; set; } = null!;
    public TermFlags Flags { get; set; }

    public Instant CreatedAt { get; set; }
    public Instant UpdatedAt { get; set; }

    public List<Tag> Tags { get; set; } = null!;
}

[Flags]
public enum TermFlags
{
    SearchHidden = 1 << 0,
    RandomHidden = 1 << 1,
    ShowWarning = 1 << 2,
    ListHidden = 1 << 3,
    Disputed = 1 << 4 // Unused, but don't want to reuse it, so keeping it here until we add another flag(?)
}

public class Tag
{
    public int Id { get; init; }
    public string Name { get; set; } = null!;

    public List<Term> Terms { get; set; } = null!;
}