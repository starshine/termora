namespace Termora.Core;

public class CoreConfig
{
    public string Database { get; set; } = null!;
    
    public string MeilisearchUrl {get;set;} = null!;
    public string MeilisearchKey {get;set;} = null!;
}