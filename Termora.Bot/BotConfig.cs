namespace Termora.Bot;

public class BotConfig
{
    public string Token { get; set; } = null!;

    public ulong? CommandsGuildId { get; set; }

    public uint EmbedColour { get; set; } = 0xD14171;
    public string? WebsiteBase { get; set; }
}