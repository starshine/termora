namespace Termora.API;

public class ApiConfig
{
    public int Port { get; set; } = 5000;
}